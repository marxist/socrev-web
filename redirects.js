module.exports = [
  {
    from: '/about-us/join-us.html',
    to: '/join-the-imt'
  },
  {
    from: '/introduction-to-the-revolutionary-philosophy-of-marxism-part-2',
    to: '/introduction-to-the-revolutionary-philosophy-of-marxism'
  },
  {
    from: '/introduction-to-the-revolutionary-philosophy-of-marxism-part-3',
    to: '/introduction-to-the-revolutionary-philosophy-of-marxism'
  },
  {
    from: '/marx-s-revolution-in-philosophy-reflections-on-the-theses-on-feuerbach',
    to: '/marxs-revolution-in-philosophy-reflections-on-the-theses-on-feuerbach'
  }
]
