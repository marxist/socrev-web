import {
  InstantSearch,
  SearchBox,
  Hits,
  Highlight,
  Stats,
  SortBy,
  Pagination,
  Configure,
} from 'react-instantsearch/dom'
import HitExcerpt from '../components/HitExcerpt'
import Layout from '../components/layout'
import { apiUrl, initStore } from '../store'
import { Provider } from 'mobx-react'
import 'isomorphic-fetch'
import SearchHeader from '../components/SearchHeader'

export default class extends React.Component {
  static async getInitialProps() {
    const res = await fetch(`${apiUrl}/categories`)
    const cats = await res.json()
    return { cats }
  }

  constructor(props) {
    super(props)
    this.store = initStore()
  }

  componentDidMount() {
    this.store.clamp()
    window.onresize = () => this.store.clamp()
  }

  render() {
    const { cats } = this.props
    return (
      <Provider store={this.store}>
        <Layout cats={cats}>
          <InstantSearch
            apiKey="93a6d17fa8b5146a0a816101d4e97f14"
            appId="JXJDRPF1XP"
            indexName="posts"
          >
            <Configure highlightPreTag="[|]" highlightPostTag="[/|]" />
            <SearchHeader>
              <SearchBox translations={{ placeholder: 'Search' }} />
              <Stats />
            </SearchHeader>
            {/*
            <SortBy
              defaultRefinement="instant_search"
              items={[
                { value: 'instant_search', label: 'Most relevant' },
                { value: 'instant_search_date_asc', label: 'Oldest' },
                { value: 'instant_search_date_desc', label: 'Newest' },
              ]}
            />
            */}
            <Hits hitComponent={HitExcerpt} />
            <Pagination showLast />
            <style jsx global>{`
              .ais-SearchBox {
                margin: 10px;
              }
              .ais-SearchBox-input {
                font-size: 20px;
              }
            `}</style>
          </InstantSearch>
        </Layout>
      </Provider>
    )
  }
}
