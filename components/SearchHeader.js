import styled from 'styled-components'

export default ({ children }) => <Wrapper>{children}</Wrapper>

const Wrapper = styled.div`
  max-width: 600px;
  margin: auto;
`
