import { Highlight } from 'react-instantsearch/dom'
//import Link from 'next/link'
import { Link } from '../routes'
import styled from 'styled-components'
//import ReactMarkdown from 'react-markdown'
import marked from 'marked'
//import elementResizeEvent from 'element-resize-event'
import { inject } from 'mobx-react'
//import * as clampy from '@clampy-js/clampy'
//import clamp from '../src/clamp'

@inject('store')
export default class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      url: '/static/imt-wil-logo.jpg',
      width: 480,
      height: 449,
    }
    const { hit } = this.props
    const img = new Image()
    img.onload = () => {
      // TODO may be causing error
      this.setState({
        url: hit.media,
        width: img.width,
        height: img.height,
      })
    }
    img.src = `${hit.media}`
  }
  decode(str) {
    //console.log(str)
    //return decodeURI(str)
    return str
  }
  plainText(str) {
    let doc = new DOMParser().parseFromString(str, 'text/html')
    return doc.body.textContent || ''
    // return he.decode(str.replace(/<[^>]+>/g, ''))
  }
  componentDidUpdate() {
    /*
    clampy.clamp(
      document.querySelector(`.handle-content-${this.props.hit.id}`),
      {
        clamp: 3,
      }
    )
    */
    /*
    const result = clamp(this.myRef, { clamp: 3 })
    console.log(result)
    */
  }
  render() {
    const { hit } = this.props
    //console.log(hit)
    hit.title = this.decode(hit.title)
    hit.excerpt = this.decode(hit.excerpt)
    hit.authors = this.decode(hit.authors)
    hit.date = this.decode(hit.date)
    hit.content = hit.content
      .filter(d => d.key === 'content')
      .map(d => this.decode(d.val))

    let title = this.plainText(marked(hit.title))
    let excerpt = this.plainText(marked(hit._highlightResult.excerpt.value))
    let author = ''
    hit.authors.forEach((a, i, l) => {
      if (i === 0) author = a
      else if (i < l.length - 1) author += `, ${a}`
      else author += ` and ${a}`
    })
    let newDate = new Date(hit.date).toString().split(' ')
    let date = newDate[1] + ' ' + newDate[2] + ', ' + newDate[3]
    // ensure hit content is relevant text from content, or exerpt
    let content = []
    if (hit._highlightResult.content) {
      content = hit._highlightResult.content
        .filter(d => d.key.value === 'content')
        .filter(d => d.val.matchLevel === 'full')
    }
    // Modify hit text for excerpt display
    if (content && content.length > 0) {
      const sentenceRe1 = /([.?!])\s+(?=[A-Za-z0-9(\["'])/g
      const sentenceRe2 = /([.?!]["'\])])\s+/g
      const highlightRe = /\[\|].+?\[\/\|]/g
      const newlineRe = /(^.)/gm
      const firstSpaceRe = /(^\s)/g
      let found = false
      let p = ''
      let sentences = []
      content.forEach(a => {
        if (!found) {
          p = this.plainText(marked(a.val.value))
          if (highlightRe.test(p)) {
            sentences = p
              .replace(newlineRe, ' $1')
              .replace(firstSpaceRe, '')
              .replace(sentenceRe1, '$1|*|')
              .replace(sentenceRe2, '$1|*|')
              .split('|*|')
            const reducer = (accumulator, currentValue) => {
              let result = accumulator
              if (currentValue.match(highlightRe)) found = true
              if (found) result = `${accumulator}${currentValue} `
              return result
            }
            content = '[...] ' + sentences.reduce(reducer, '')
          } else content = excerpt
        }
      })
    } else content = excerpt

    let text =
      hit._highlightResult.excerpt.matchLevel === 'full' ? excerpt : content
    text = text
      .replace(/\[\|]/g, '<em class="search-highlight">')
      .replace(/\[\/\|]/g, '</em>')

    // Limit amount of text for cases in which it's too long (
    let words = text.split(' ')
    text = words.length > 50 ? words.slice(0, 50).join(' ') + ' [...]' : text

    // Reference for resize function
    this.content = text

    // image within svg
    // http://tutorials.jenkov.com/svg/svg-viewport-view-box.html
    let img =
      this.state.url === '/static/imt-wil-logo.jpg' ? null : (
        <SvgWrapper>
          <svg
            width="180"
            height="80"
            viewBox={`0 0 ${this.state.width} ${this.state.height}`}
            preserveAspectRatio="xMidYMid slice"
            aria-labelledby="title"
          >
            <title id="title">{hit.title}</title>
            <image
              xlinkHref={this.state.url}
              x="0"
              y="0"
              width="100%"
              height="100%"
            />
          </svg>
        </SvgWrapper>
      )
    return (
      <div>
        <Divider />
        <Link prefetch route={`/${hit.slug}`} passHref>
          <A>
            <FixCenter>
            <SearchExcerpt>
              <Title>{title}</Title>
              <Byline>
                {author}, {date}
              </Byline>
              <SearchContent>
                {img}
                {/*<Highlight attribute="excerpt" hit={hit} />*/}
                {/*<ReactMarkdown source={content} />*/}
                {/*className={`handle-content ${hit.slug}`}*/}
                <div
                  ref={el => (this.myRef = el)}
                  className={`handle-content-${hit.id}`}
                  dangerouslySetInnerHTML={{ __html: text }}
                />
              </SearchContent>
            </SearchExcerpt>
            </FixCenter>
            <style jsx>{`
              img {
                width: 100px;
              }
            `}</style>
            {/*
          <style jsx global>{`
            .ais-Hits-item {
              margin-top: 15px;
            }
          `}</style>
          */}
          </A>
        </Link>
      </div>
    )
  }
}

const A = styled.a`
  color: inherit;
  text-decoration: none;
  &:visited {
    color: inherit;
  }
  &:hover {
    color: #600000;
  }
  @media (min-width: 720px) {
    display: flex;
    justify-content: center;
  }
`
const SearchExcerpt = styled.div`
  display: flex;
  flex-flow: column;
  margin: 10px;
  max-width: inherit;
  @media (min-width: 720px) {
    /*
    margin: 20px 70px 0px 70px;
    */
    max-width: 800px;
  }
`
const SearchContent = styled.div`
  display: flex;
  flex-flow: column;
  @media (min-width: 720px) {
    flex-flow: row;
  }
`
const Content = styled.div`
  display: flex;
  /*flex-flow: column;*/
`
const SvgWrapper = styled.div`
  margin: 8px auto;
  @media (min-width: 720px) {
    margin: 8px;
    margin-left: 0px;
  }
`
const Title = styled.div`
  font-family: Mada, sans-serif;
  line-height: 1.2;
  font-weight: bold;
  align-self: center;
  @media (min-width: 720px) {
    align-self: inherit;
  }
`
const Divider = styled.div`
  height: 1px;
  background-color: #c9c9c9;
`

const Byline = styled.div`
  font-size: 80%;
  text-align: center;
  @media (min-width: 720px) {
    text-align: inherit;
  }
`

const FixCenter = styled.div`
  width: 100vw;
  @media (min-width: 800px) {
    width: 800px;
  }
`
