const express = require('express')
const next = require('next')
const { join } = require('path')
const mobxReact = require('mobx-react')
const fetch = require('isomorphic-fetch')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const thisHost = 'https://socialistrevolution.org'

// next-routes
const routes = require('./routes')
const handler = routes.getRequestHandler(app)

mobxReact.useStaticRendering(true)

const redirects = require('./redirects')

const rootStaticFiles = [
  '/favicon.ico',
  '/apple-touch-icon.png',
  '/apple-touch-icon-composed.png',
  '/robots.txt',
  '/socrev-logo-stacked.png',
  '/srlogo.png',
]

app.prepare().then(() => {
  const server = express()

  // static files
  const options = { root: join(__dirname, 'static') }
  rootStaticFiles.forEach(file => {
    server.get(file, (req, res) => {
      res.sendFile(file.slice(1), options)
    })
  })

  server.get('*', async (req, res, next) => {
    const host = req.get('host')
    if(req.url.includes('/mailman')) {
      const mailmanUrl = `http://wp.socialistrevolution.org${req.url}`
      res.redirect(301, mailmanUrl)
    }
    // handles redirects for socialistappeal.org requests
    if (host.includes('socialistappeal')) {
      // grab id from url
      const re = /(\/[0-9])\w+/
      const match = req.url.match(re)
      let id
      if (match !== null) {
        id = match[0].substring(1)
      } else {
        console.log(`no id found in ${req.url}`)
        // no id found on SA request, send to SR index
        res.redirect(301, thisHost)
      }
      console.log(
        'fetching',
        `https://socrev-db-vbqykbqlje.now.sh/slug?id=${id}`
      )
      const response = await fetch(
        `https://socrev-db-vbqykbqlje.now.sh/slug?id=${id}`
      )
      const json = await response.json()
      res.redirect(301, `${thisHost}/${json.slug}`)
    } else next()
  })

  // redirects
  redirects.forEach(({ from, to, type = 301, method = 'get' }) => {
    server[method](from, (req, res) => {
      res.redirect(type, to)
    })
  })

  // dynamic routing via next-routes
  server.use(handler)

  server.listen(3000, err => {
    if (err) throw err
    console.log('> ready on port 3000')
  })
})
